---
title: About
subtite: Technical Lead, Developer, Trainer, Speaker & Consultant
comments: false
type: page

---
**Who am I?**

Hi there! I am David McKay, a software and technology consultant, born and bred in Glasgow, Scotland.

I enjoy working with containers, Docker or rkt, on AWS and organising many user-groups!

I also have a wealth of experience workiong with Android, PHP and iOS, and have a keen interest in BDD, DDD and TDD, as well as SaltStack, Microservices, CQRS and EventSourcing

**User Groups**

I founded, and organise:

* Cloud Native Glasgow
* DevOps Glasgow
* mongoDB Glasgow
* Pair Progamming Glasgow

I also founded Docker Glasgow, but I have since stepped down; to focus on more rounded / encapsulating groups.

Check-out my <a href="https://www.meetup.com/members/153009982/">Meetup.com</a> page for more information!

**What is Rawkode?**

Rawkode is a pseudonym I use. I don't use it for any kind of anonymity - it's purely a screen name I made up while frustrated at not being able to register "David" on most websites. It combines my two passions: rock music & code.

**I have two passions?**

Actually, I have three! I didn't think it would be professional enough to list Old Fashioned's though.

**How long have I been developing software?**

I started coding when I was thirteen years-old, working with the wonderful COMAL, Smalltalk and C programming languages. I was twenty-two before I started to develop real systems; twenty six before I started developing good systems.
More Information?

I have been designing and developing software for over ten years. During this time I have worn a lot of hats and acquired many skills that make me the specialist I am today. I am an experienced PHP, Java and Objective-C developer: specialising in Symfony2, Android and iOS.

**Software Architecture & Development**

I utilise service-oriented architecture and agile methodologies to deliver scalable, testable, robust systems. Agile methodologies have allowed me, and my teams, to fully embrace change in project scope and requirement - yet still delivering systems on-time, on-budget and to-specification.

Service-Oriented Architecture is a paradigm of breaking down large enterprise systems into small manageable, self contained components. This approach to systems design allows each individual component to be upgraded, at will, without breaking the entire system - facilitating fast uptake of new and exciting technologies.

**DevOps**

I have been managing Linux systems since I installed Coral Linux on my first ever PC, back in 2000. Although it was pretty awful, it got me involved with Debian and I've never looked back since. I am an advocate of the command-line and love whipping up bash scripts, but with my ever growing responsibilities I have embraced the recent surge of the DevOps culture and I have embraced tools such as SaltStack and Ansible. I am a big fan of Continuous Integration & Delivery - nobody should ever fear a Friday evening deployment.

**Buzzwords**

* Agile Development
* Amazon Web Services (AWS) (Solution Architect Certified)
* Cloud Native
* Continuous Integration & Delivery
* Docker
* Doctrine
* Elasticsearch
* Elixir
* Fluentd
* Git
* Golang / Go
* Kafka
* Kubernetes
* Linux (Debian, RedHat, Arch)
* Logstash
* memcached
* Micro-services (Service-Oriented Architectures)
* MySQL / MariaDB
* Neo4J
* NoSQL - DynamoDB / MongoDB / Redis
* PHP (5 & 7)
* PostgreSQL
* SaltStack
* Symfony
* Vagrant
* Vanish

**Why Have I Worked With So Many Languages / Frameworks / Platforms?**

I'm a developer, I use the right tools for the job, not merely those at my disposal. If I am not familiar with the right tools, language or platform - I'll learn it. As well as being my career, this is also my passion. Learning and honing my skills is a full time job, and as such I utilise as much of my spare time as I can, to further my knowledge and abilities.