---
draft: true

title: "The Mindful Developer: Automating Your Day with Habits &amp; Routines"
subtitle: "The Power of Habits"
date: 2017-04-11

series: ["The Mindful Developer"]
categories: ["Productivity"]
tags: ["Habits", "Routines", "Willpower"]

---

# How I Work - Automating Your Day with Habits & Routines

## Starting my Day

My day starts the night before ... yep! Mornings are not a good time for most people, so don't make is strenuous on your mind. I like to make sure that my mornings are mostly automated through habits and routines.

### Habits & Routines

When I say "habit", I am talking about one single abuse of "the habit loop" to automate a single task.

#### Example

* **Cue:** Waking up
* **Action:** Set kettle to 80° celsius
* **Reward:** A lovely cup of coffee

You'll often see "*Action*" referred to as "*Routine*" by those that have read "*The Power of Habit*", as this is what Charles Duhigg calls it. However, I prefer to consider "*Routine*" the chaining of habits to provide a morning "*Routine*", and as such - the rest of this article will use habit and routine is this form.

My morning routine chains habits together and looks like the following:

* Wake up
* Put on kettle
* Shower in horribly cold water
* Drink delicious coffee
* Meditate
* Get dressed
* Leave the house

Things of note:

* I wake up at 0530
* There is no phone or television in my morning routine.
* I shower in warm water for 5 minutes and then cold water for 5 minutes (Article coming soon)
* My morning meditation practice is guided by Headspace

## Throughout my Day

## Finishing my Day
