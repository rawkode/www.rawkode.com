---
title: "The Mindful Developer: Blending Digital & Physical"
subtitle: "Utilising Notebooks & Computers for Productivity Bliss"
date: 2017-04-04

series: ["The Mindful Developer"]
categories: ["Productivity"]
tags: ["Bullet Journal", "Rapid Logging", "Code&Quill", "TickTick", "Google Drive", "Lamy"]

---

This post is the first in a series, "The Mindful Developer". Can't be bothered waiting for the other instalments? My <a href="https://speakerdeck.com/rawkode/the-mindful-developers-being-less-busy-and-more-productive-midwestphp-2017">slides from MidwestPHP 2017</a> might be better suited.

# Blending Digital &amp; Physical

Being a computer programmer, you'd assume that I often reach for a keyboard before a pen and paper ... that's not necessarily the case. In this article, I will explain how I use a combination of physical and digital tools to keep my life organised.

## Why Physical?

Throughout the day, most people will generally have their phone on them. It's always vibrating and beeping, telling you that your BFF has posted a photo of their dinner to their Instagram page and you **MUST SEE IT NOW**.

Unless you've got custom ring tones and notifications configured, you won't know if that most recent beep was someone you love being rushed to hospital, or another frivolous status update from an acquaintance of an acquaintance. Unfortunately, as soon as you've unlocked your phone; you've kicked off a habit loop that will probably see you drifting from Twitter, Facebook, Google Mail and Wikipedia ... in no particular order. Your mind knows that you're going to get little hits of dopamine when you check these applications and it'll take a lot of willpower to stop it.

## Why Digital

Digital systems are great. When it comes to reporting and course correcting, you're going to need a digital system. You don't want to be flipping through hundreds of pages of paper to start establishing trends and patterns. Computers are **GOOD** at this, we are not.

## My System

So lets dive in! Let me walk you through my system and the tools I use.

### The Notebook

At the moment, I am using a Code &amp; Quill traveller. It's a fantastic notebook with two page styles. All the pages on the left hand side are dotted and all the pages on the right are lined, with markers.

{{< load-photoswipe >}}
{{< gallery caption-effect="fade" >}}
  {{< figure caption-position="center" caption-effect="slide" caption="Notebook Cover" link="/img/how-i-work-blending-digital-physical/notebook-cover.jpg" >}}
  {{< figure caption-position="center" caption-effect="slide" caption="Notebook Left Side" link="/img/how-i-work-blending-digital-physical/notebook-left.jpg" >}}
  {{< figure caption-position="center" caption-effect="slide" caption="Notebook Right Side" link="/img/how-i-work-blending-digital-physical/notebook-right.jpg" >}}
  {{< figure caption-position="center" caption-effect="slide" caption="Notebook Both Sides" link="/img/how-i-work-blending-digital-physical/notebook-both.jpg" >}}
{{< /gallery >}}

This combination is perfect for me. I use the right for my rapid logging and the left for images / diagrams, when/if required.

### The Pens

If you're going to write on paper, make sure you are going to enjoy every second of it. For me, that means using a fountain pen. I use a bunch of Lamy Safari's. Great pens at a very affordable price.

{{< figure src="/img/how-i-work-blending-digital-physical/pens.jpg" caption="Lamy Safari Fountain Pens" width="100%" caption-position="bottom" caption-effect="fade">}}

### Rapid Logging

In-order to take notes throughout the day, especially during meetings, you need to learn how to write concisely and quickly. For that, I use "Rapid Logging", a technique that makes up the Bullet Journalling system.

#### What is Rapid Logging?

Rapid logging is a style of note-taking that allows you to define "bullets". These bullets provide context to your thoughts.

##### My Key

I like to keep my key pretty simple. I have a sigil for: tasks, notes, questions, expenses and gratitude.

{{< figure src="/img/how-i-work-blending-digital-physical/rapid-logging-key.jpg" caption="Rapid Logging Key" width="100%" caption-position="bottom" caption-effect="fade">}}

### TickTick

Now the digital side of my system, [TickTick](http://www.ticktick.com).

I've tried **every** to-do list program out there. I hate them all so much, I'm currently building my own. I'll talk about this later. TickTick is the one I am happiest with, for now. There are a few reasons I'm currently using this over it's competitors:

* It's web based and mobile (Android and iOS)
* It has advanced filters
* It has an "Inbox"
* It has text based input through it's application and Google Now / Assistant (for when I'm driving)
* It has a "review" ability, allowing me to see what I've done during the last week, month, and year


### Google Drive

My Google Drive is broken into sections:

* **Active Projects**: Symlinks (Shift-z) to "Projects"
* **Correspondence**: All mail I've received from companies
* **Inbox**: I store all new documents here and I'll put them in their correct place during my reviews
* **Notes**: Notes on subjects I've studied: Comp Sci, Mythology, Card Magic ... anything I want to remember!
* **Projects**: Notes and minutes from projects I've been involved in
* **Public**: Shared resources
* **Vault**: PGP encrypted files that I've not decided where to store properly yet

{{< gallery caption-effect="fade" >}}
  {{< figure caption-position="center" caption-effect="slide" caption="Google Drive" link="/img/how-i-work-blending-digital-physical/drive.png" >}}
  {{< figure caption-position="center" caption-effect="slide" caption="Google Drive Correspondence" link="/img/how-i-work-blending-digital-physical/drive-correspondence.png" >}}
  {{< figure caption-position="center" caption-effect="slide" caption="Google Drive Correspondence (by date)" link="/img/how-i-work-blending-digital-physical/drive-correspondence-by-date.png" >}}
{{< /gallery >}}

Every piece of mail I get is scanned and archived in "Correspondence". Occasionally, some emails will be saved to PDF and added to this directory.

All notes will be stored within "Notes", unless they're "Project" specific, in which they live inside "Projects"

### In Practice

So how does these all fit together?

#### The Daily Review

Every night, I sit down in-front of my computer with my notebook and:

1. Any tasks completed in my notebook get marked as completed in TickTick
2. Any tasks yet to be completed are marked as rescheduled, >, in my notebook, and scheduled for tomorrow in TickTick
3. Any new thoughts, notes, questions, etc, are placed into their digital home
  1. Notes are moved to Google Drive
  2. Tasks into TickTick
4. I look at my backlog in TickTick and schedule tasks for tomorrow
  1. Ensuring at-least one task scheduled for tomorrow moves me forward with my weekly goal
5. All tasks scheduled for tomorrow in TickTick are added to a new day in my notebook

#### The Weekly Review

Every Sunday night, I extend my daily review by adding a few new tasks:

1. Review calendar for the next month and add tasks and reminders for anything that doesn't have them, but needs them
2. Set a weekly goal, that should contribute to one my monthly or quarterly goals.

## That's It!

Hopefully I've not missed anything. If you've got any questions, or are looking for advice at developing your own system; comment away or grab me on Twitter.

Best of luck!
